import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useEffect } from "react";
import phoneIcon from './assets/phone.png';

const Click2Call = () => {
  const [data, setData] = useState([]);
  const [title, setTitle] = useState(["Call"]);

  useEffect(() => {
      fetch(global.config.variables.api_url_base + "/click2call?_format=json")
        .then((res) => res.json())
        .then((data) => {
          setData(data.field_number_clicktocall);
          setTitle(data.title)
})
  }, []);
  return (
    <>
        { data && data.map((item:any, index:number) =>(
            <a className="click-to-call" href={"tel:"+item.value}>
                { title && title.map((item:any,index:number) => (
                  item.value
                ))}
            </a>
        ))}
    </>
  );
};

export default Click2Call;

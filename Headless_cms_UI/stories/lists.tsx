import { useState } from "react";
import { SSRProvider, Table } from "react-bootstrap";

interface tableData{
  data:any
}

const UserList = (props:tableData) => {
  console.log(props.data[0], 'data')
    return(
      <SSRProvider>
        <Table striped bordered hover variant="dark">
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Company</th>
      <th>Country</th>
    </tr>
  </thead>
  <tbody>
    {props.data.map((dataItem, index) => 
          <tr key={index + 1}>
           
            <td>{index + 1}</td>
            <td>{dataItem.title}</td>
      <td>{dataItem.field_company}</td>
      <td>{dataItem.field_country}</td>
      </tr>
     )} 
     </tbody>
</Table>
</SSRProvider>
    )
}

export default UserList;
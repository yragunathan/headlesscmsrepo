import React from 'react';

interface cardProps{
    content?:string
}

const FreeContent=(props: cardProps) => {
    return(
        <div dangerouslySetInnerHTML={{__html: props.content}}>
       </div>
    )
    };
    
export default FreeContent;


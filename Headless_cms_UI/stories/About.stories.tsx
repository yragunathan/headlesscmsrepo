import React from 'react';
import { Story, Meta } from '@storybook/react';
import { within, userEvent } from '@storybook/testing-library';
import About from './About';

export default {
  title: 'Molecules/About',
  component: About,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} as Meta<typeof About>;

const Template: Story<typeof About> = (args) => <About {...args} />;

export const about = Template.bind({});
about.args = {
    "BothAboutAndMoreData": {
        "AboutData": {
            "body": [ {
                    "value": "<p>Suspendissed convallis dapibus lobortis. Nunc a pulvinar nulla, ac ornare tortor. Morbi sed condimentum dolor. Quisque tristique at ante ut posuere. Duis imperdiet mollis efficitur. Ut non diam malesuada, pellentesque metus eget, rhoncus justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum pulvinar nunc vel neque aliquet, vel placerat est ultrices. Maecenas dapibus neque leo, sit amet commodo tellus congue ut.</p>", },
            ],
        },
        "AboutReadMore": {
            "body": [ {
                    "value": "<p>Praesent elit lacus, cursus eget rutrum a, faucibus in mauris. Mauris fringilla quam id leo eleifend, vitae vulputate velit placerat. Etiam eleifend turpis urna, ut tristique augue bibendum ut. Duis porttitor maximus diam, ut posuere mauris eleifend eget. Cras eget tempus ante, id viverra erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat sit amet tortor eget condimentum.</p>",
                },
            ],
        },
    },
}
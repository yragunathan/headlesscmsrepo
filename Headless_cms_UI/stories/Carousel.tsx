import React, { useEffect } from "react";
import Slider from "react-slick";
import SliderWrapper from "./slickStyles";
import Image from "next/image";
import { useState } from "react";

const IPharmaCarousel = (props) => {
  const [data,setData] = useState([]);
    const settings = {
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        speed: 500,
        arrows: true,
        adaptiveHeight: true,
        appendDots: (dots:any) => <ul>{dots}</ul>,
        customPaging: (i:any) => (
          <div className="ft-slick__dots--custom">
            <div className="loading" />
          </div>
        )
      };
    
    
    return(
        <div className="slider">
        <SliderWrapper>
        <Slider {...settings}>
          {props.BannerData && props.BannerData.Banner && props.BannerData.Banner.map((item,index) => 
            <div  key={index}  className="testimoni--wrapper">
            <Image src={`${global.config.variables.api_url_base}${item.field_banner_post}`}  layout="responsive"  priority alt='car-1' width={1400} height={400} />
            </div>
            
          )

          }
        </Slider>
      </SliderWrapper>
      </div>
    )
}

export default IPharmaCarousel;
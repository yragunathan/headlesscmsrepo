import React, { useEffect, useState } from "react";
import { Accordion, Modal } from "react-bootstrap";

interface Iprops {
  isOpen: boolean;
  close: Function;
  data: any;
}

function FavouriteButtonModal(props: Iprops) {
  const [show, setShow] = useState(props.isOpen);
  const [data, setData] = useState(props.data);

  const handleClose = () => setShow(props.close());

  return (
    <>
      <Modal
        show={props.isOpen}
        onHide={handleClose}
        dialogClassName="modal-90w"
        aria-labelledby="Innvotive Medicine"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Innvotive Medicine
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            dangerouslySetInnerHTML={{ __html: props.data ? props.data : "" }}
          ></div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default FavouriteButtonModal;

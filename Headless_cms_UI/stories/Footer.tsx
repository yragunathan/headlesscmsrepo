import React from "react";
import paypal from "../public/assets/paypal.png";
import mastercard from "../public/assets/mastercard.png";
import visa from "../public/assets/visa-icon.png";
import amex from "../public/assets/amex.png";
import logo from '../public/assets/logo.png';
import Image from 'next/image';
import Click2Call from "./Click2Call";
import { Col, Row, Container } from "react-bootstrap";

const Footer = () => (
  <footer>
    <div className="footerClass">
      <Container>
      <Row style={{textAlign:"center"}}>
          <Col>
            <Click2Call />
          </Col>
      </Row>
      <Row style={{alignItems:"center"}}>
      <Col xs={5} sm={6} style={{textAlign:"right"}}>
        <Image
          src={logo}
          width={150}
          style ={{float:"right"}}
          height={30}
          alt="iPharma"
          loading="lazy"
        />
      </Col>
    
      <Col xs={7} sm={6}>  Altimetrik. All rights reserved 2022</Col>
      </Row>
      </Container>
      {/* <span>
        <Image src={paypal} className={"m-r-5"} alt={"PAYPAL"} width={50} height={25}/>
        <Image
          src={mastercard}
          className={"m-r-5"}
          alt={"master-card"}
          width={35}
        />
        <Image src={visa} className={"m-r-5"} alt={"master-card"} width={60} />
        <Image src={amex} className={"m-r-5"} alt={"master-card"} width={55} />
      </span> */}
    </div>
  </footer>
);

export default Footer;

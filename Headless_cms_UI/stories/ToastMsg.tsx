import { ToastContainer, Toast, CloseButton } from "react-bootstrap";

interface ToastProps{
    id:string;
    msg:string;
    show?:boolean;
    onClose?:Function;
}

const ToastMsg = (props:ToastProps) => {
return(
<ToastContainer className="p-t-30" position="top-center">
  <Toast>
    <Toast.Body>
        <div>
        {props.msg}
        </div></Toast.Body>
  </Toast>
  </ToastContainer>
)
}

export default ToastMsg;
export interface LoginData{
    username: string;
        password: string,
        client_secret: string,
        client_id : string,
        grant_type: string,
        scope: string
}
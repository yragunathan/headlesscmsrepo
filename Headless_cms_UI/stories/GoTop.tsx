import React, { useEffect } from "react";
import backtotopImg from './assets/backtotop.png'

const GoTop = () => {
    useEffect(() => {
        let scrollButton = document.getElementById("backToTop")
        window.onscroll = function() {
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                scrollButton.style.display = "block";
            }
            else {
                scrollButton.style.display = "none";
            }
        }
    })

    return(
        <>
            <a href="#" id={"backToTop"}><img src={backtotopImg.src} /></a>
        </>
    )
}

export default GoTop;
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { within, userEvent } from '@storybook/testing-library';
import FreeContent from './FreeContent';


export default {
  title: 'Molecules/FreeContent',
  component: FreeContent,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof FreeContent>;

const Template: ComponentStory<typeof FreeContent> = (args) => <FreeContent {...args} />;

export const freeContent = Template.bind({});

freeContent.args = {
    content: "<div class='col-6 offset-3'><p>This is a paragraph</p><h1>This is a header</h1><h2>This is a header</h2><h3>This is a header</h3><small>Small text</small></div>"
}
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { IHeader } from './Header';

export default {
  title: 'Molecules/Header',
  component: IHeader,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof IHeader>;

const Template: ComponentStory<typeof IHeader> = (args) => <IHeader {...args} />;

/*export const LoggedIn = Template.bind({});
LoggedIn.args = {
  user: {
    name: 'Jane Doe',
  },
};

export const LoggedOut = Template.bind({});
LoggedOut.args = {};*/
export const header = Template.bind({})

import React from 'react';
import { Story, Meta } from '@storybook/react';

import Accordian from './Accordian';

export default {
  title: 'Molecules/Accordian',
  component: Accordian,
  
} as Meta;

//const Template: any = () => <Accordian open={false} />;
const Template: Story = (args) => <Accordian open={false} {...args} />;

export const accordian = Template.bind({})

accordian.args = {
  AccordianData : [
    {"title": "Accordian 1", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
    {"title": "Accordian 2", "body": "Nunc fermentum erat enim, ut pellentesque lectus egestas sed. Aliquam suscipit dignissim euismod. Sed condimentum commodo turpis in venenatis. Vestibulum blandit pulvinar ante, vitae accumsan urna. Ut purus arcu, sagittis sed feugiat at, bibendum et nulla. Nunc elementum fringilla risus, in lacinia augue lacinia quis. Suspendisse malesuada molestie massa, vel vehicula erat facilisis sed. Mauris consequat et velit sed consequat. Nulla nec cursus augue, nec porta leo. Phasellus sed porttitor urna, vel scelerisque risus. Phasellus elementum eget mauris at mattis. Aliquam et nisi lacinia, sollicitudin tortor non, consectetur enim. Sed porttitor sodales molestie. Fusce vel dui eu nisl consequat tempor in a sem. Nullam lobortis consequat lacus sit amet bibendum. Donec nec semper mi.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
    {"title": "Accordian 3", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
  ]
}
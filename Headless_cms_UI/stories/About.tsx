import IPharmaCarousel from "../stories/Carousel";
import Footer from "../stories/Footer";
import { IHeader } from "../stories/Header";
import NavBar from "../stories/NavBar";
import Modal from "../stories/Modal";
import React, { useState } from "react";
import { useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import TabbedContent from "./TabbedContent";

interface Iprops {
  open: boolean;
}

function About(BothAboutAndMoreData,props: Iprops) {
  const [modalOpen, setModalOpen] = useState(false);
  const setModaltoOpenTrue = () => {
    setModalOpen(true);
  };
  const setModaltoOpenFalse = () => {
    setModalOpen(false);
  };
  const [show, setShow] = useState(props.open);
  




  return (
    <>
      {BothAboutAndMoreData.BothAboutAndMoreData.AboutData && BothAboutAndMoreData.BothAboutAndMoreData.AboutData.body &&
        BothAboutAndMoreData.BothAboutAndMoreData.AboutData.body.map((about: any, index: number) => (
          <>
            <Container>
            <Row dangerouslySetInnerHTML = {{__html:about.value}}></Row>
              <button
              onClick={() => {
                setModaltoOpenTrue();
              }}
            >
              {">>"} Read More
            </button>
            </Container>
            <Modal isOpen={modalOpen} close={setModaltoOpenFalse} data={BothAboutAndMoreData.BothAboutAndMoreData.AboutReadMore && BothAboutAndMoreData.BothAboutAndMoreData.AboutReadMore.body && BothAboutAndMoreData.BothAboutAndMoreData.AboutReadMore.body[0].value} />
          </>
        ))}
        <Container> <TabbedContent /> </Container> 
    </>
  );
}

export default About;

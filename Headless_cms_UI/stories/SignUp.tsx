import { SyntheticEvent, useEffect, useMemo, useState } from "react";
import { Alert, Button, Col, Form, FormControlProps, Modal, Row } from "react-bootstrap";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

interface SignupProps {
    open: boolean;
    close: Function;
   }

const SignUp = (props: SignupProps) => {
    const [show, setShow] = useState(props.open);
    
    const [signUpResp, setSignUpresp] = useState({} as any);
    const [formData, setFormData] = useState({
      
        mail: {
          value: ''
        },
        name: {
          value: ''
        },
        pass: {
          value: ''
        }
    });
    const {Group, Label, Control} = {...Form}
    const handleClose = () => {
      setShow(props.close());
      setFormData({...formData, name: {value: ''}, mail: {value:''}, pass: {value: ''}})
      reset();
      setSignUpresp({});
   }
  
    const SignupSchema = yup.object().shape({
      "UserName": yup.string().required().max(25, "User name can only have 25 characters").matches(/^[a-z]+-?\d*$/i, "User name can only have alphanumeric value"),
      "email": yup.string().email('Must be a valid email').max(255).required('Email is required'),
      "password": yup.string().required('Password is required').min(8, "Password must be 8 characters long").max(20, "Password must be maximum 20 characters long"),
      "passwordConfirmation": yup.string().oneOf([yup.ref('password'), null], 'Passwords must match')

    })

    const { register, handleSubmit,reset, formState:{errors} } = useForm({resolver: yupResolver(SignupSchema)});
  const onSubmit = (data, e) => {
    const signUpData = {...formData, name: {value : data.UserName}, mail: {value : data.email}, pass: {value : data.password}};

    fetch(global.config.variables.api_url_base+"/user/register?_format=json", {
      method: 'POST',
      headers:{
        "content-type" : "application/json",
        "X-CSRF-Token" : "XNWao-ReNnHkXVobxZgI31sFkAuo3sSxmw45zoaTNo8"
      },
       body: JSON.stringify(signUpData),
  }).then((res) => setSignUpresp(res))
  reset();
  }

  const onError = (errors) => {
    console.error(errors);
  };
    return (
        <>
       
          <Modal show={props.open} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>SignUp</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <>
            
            <form  onSubmit={ handleSubmit(onSubmit, onError )}>
          <div className="form">
          <div className="form-body">
          {signUpResp.status === 200 ?
            <Alert variant="success">
              Registered Successfully!! Please login
            </Alert>
            : null}
            {signUpResp && String(signUpResp.status)[0] === '4'  ?
            <Alert variant="danger">
              {/* {signUpResp.statusText} */}
              The username/ email address you have entered is already registered.
            </Alert>
            : null
        }
            <Group>
       <Label>User Name</Label>
       <Control className="input"
         id="name"
         name='name'
         placeholder="User Name"
         type="text"
          {...register('UserName')}
        />
         {errors.UserName && <p className="errors">{errors.UserName.message}</p>}
        {/* <div>{errors.name?.message}</div> */}
        </Group>
        <Group>
       <Label>Email</Label>
       <Control className="input"
         id="email"
         name="mail"
         type="email"
         placeholder="email"
         {...register('email')}
       />
       {errors.email && <p className="errors">{errors.email.message}</p>}
       {/* <div>{errors.email?.message}</div> */}
       </Group>
       <Group>
  <Label>Password</Label>
       <Control className="input"
         id="pass"
         name="pass"
         type="password"
         placeholder="password"
         {...register('password')}
       />
       {errors.password && <p className="errors">{errors.password.message}</p>}
       {/* <div>{errors.pass?.message}</div>  */}
       </Group>
       <Group>
  <Label>Confirm Password</Label>
       <Control className="input"
         id="cnfmPassword"
         name="cnfmPassword"
         type="password"
         placeholder="confirm password"
         {...register('passwordConfirmation')}
       />
       {errors.passwordConfirmation && <p className="errors">{errors.passwordConfirmation.message}</p>}
       {/* <div>{errors.cnfmPassword?.message}</div> */}
       </Group>
      
       <button className="btn" type={'submit'}>Submit</button>
           </div>
           </div>
 
            </form>
            </>
            </Modal.Body>
           </Modal>
        </>
      );
}

export default SignUp;

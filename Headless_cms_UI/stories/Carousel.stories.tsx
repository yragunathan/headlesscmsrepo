import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import IPharmaCarousel from './Carousel';

export default {
  title: 'Molecules/Banner',
  component: IPharmaCarousel,
  
} as ComponentMeta<typeof IPharmaCarousel>;

const Template: ComponentStory<typeof IPharmaCarousel> = (args) => <IPharmaCarousel {...args} />;

export const carousel = Template.bind({})

carousel.args = {
  BannerData: [
    {"field_banner_post":"\/sites\/default\/files\/inline-images\/ban-1.png.jpeg"},{"field_banner_post":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"}
  ]
}
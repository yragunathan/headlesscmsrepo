import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Click2Call from './Click2Call';

export default {
  title: 'Molecules/Click2Call',
  component: Click2Call,
  
} as ComponentMeta<typeof Click2Call>;

const Template: ComponentStory<typeof Click2Call> = () => <Click2Call />;

export const click2call = Template.bind({})

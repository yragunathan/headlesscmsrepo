import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { within, userEvent } from '@storybook/testing-library';
import SalesCards from './SalesCards';


export default {
  title: 'Molecules/SalesCards',
  component: SalesCards,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof SalesCards>;

const Template: ComponentStory<typeof SalesCards> = () => <SalesCards/>;

export const salescards = Template.bind({});


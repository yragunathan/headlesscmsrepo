import Link from "next/link";
import { useCallback, useEffect, useState } from "react";
import { Alert, Button, Form, FormControlProps, Modal } from "react-bootstrap";
import ForgotPassword from "./ForgotPassword";
import { LoginData } from "./types/App.types";

interface LoginProps {
  open: boolean;
  close: Function;
  handleLoginData:Function;
  loginForm: Function;
  loginResp?: any;
}

const Login = (props: LoginProps) => {
  const [show, setShow] = useState(props.open);
  const [forgotPwd, setforgotPwd] = useState(false);
  const [formData, setFormData] = useState({
    username: "",
    password: "",
    client_secret: "admin@123",
    client_id: "972c1e32-53eb-4aaa-9e44-5f5ca6f95cc2",
    grant_type: "password",
    scope: "rest_user",
  } as LoginData);
  const handleClose = useCallback(() => setShow(props.close()),[props]);
  const handleNameChange = (e: any) => {
    setFormData({ ...formData, username: e.target.value });
  };
  const handlePwdChange = (e: any) => {
    setFormData({
      ...formData,
      password: e.target.value,
    });
  }; 
  const loginForm = () => {
    props.loginForm(formData);
   // handleClose();
  }

  const handleForgotPwd= () => {
    setforgotPwd(true);
     handleClose();
  }
  const handlePwdClose = () => {
    setforgotPwd(false)
  };

 
  useEffect(()=>{
    if(props.loginResp.status === 200){
      handleClose();
    }
  }, [handleClose, props.loginResp])
  useEffect(() => {
    if(formData.username !== '' && formData.password !== '' && formData.client_id !== '' && formData.client_secret !== '' && formData.grant_type !== '') {
      props.handleLoginData(formData)
    }
  },[formData, props])
  return (
    <>
      <Modal show={props.open} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {props.loginResp.status === 400 ?
              <Alert variant="danger">
                Incorrect credentials! Please try again...
              </Alert>
              : null}
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                name="username"
                onChange={handleNameChange}
                placeholder="Enter username" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                onChange={handlePwdChange}
                name="password"
                placeholder="Password" />
            </Form.Group>
            <div onClick={handleForgotPwd}>Forgot Password</div>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={loginForm}>
            Login
          </Button>
        </Modal.Footer>
      </Modal>
      <ForgotPassword open={forgotPwd} handleClose={handlePwdClose}/>
    </>
    
  );
};

export default Login;

import React, { useEffect, useState } from 'react';

interface heroProps{
    content?:string
}

const HeroText = (props: heroProps) => {

    const [data, setData] = useState([] as any);
  
    useEffect(() => {
        fetch(global.config.variables.api_url_base + "/hero_text?_format=json")
          .then((res) => res.json())
          .then((data) => {
            setData(data.body);

  })
    }, []);

    console.log(data);
    return(
        
      <div className="jumbotron" style={{"padding": '16px 16px',
      "marginBottom": '32px',
      "backgroundColor": '#e9ecef',
      "borderRadius": '4.8px'}}>
      <h1 className="display-4">Hello, world!</h1>
      <p className ="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
      <h3 dangerouslySetInnerHTML={{__html: data && data[0]?.value}}>
       </h3>    
        </div>
    )
    };
    
export default HeroText;


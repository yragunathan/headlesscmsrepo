import Search from './Search';
import Image from 'next/image';
import logo from '../public/assets/logo.png'
import { Col, Row, Nav } from 'react-bootstrap';
import { useState ,useEffect} from 'react';
import FavouriteButtonModal from './Modal';
import Link from 'next/link';

interface Iprops{
  user?:any;
  loggedIn :boolean;
  className?:string;
}

const NavBar = (props:Iprops) => {
  const [openModal, setOpenModal] = useState(false);

  const handleClick = () => {
    setOpenModal(true)
  }

  const [menu, setMenu] = useState([]);
  const url = global.config.variables.api_url_base + "/api/menu_items/main";
  let bearer= ""
  if ( typeof window !== "undefined"){
    bearer =localStorage.getItem('token') === null ? '' : JSON.parse(localStorage.getItem('token'))?.access_token;
  }
 
  useEffect(() => {
    bearer = 'Bearer ' + bearer;
    if (props.loggedIn === true){
		fetch(url, {
      method: 'GET',
      headers: {
          'Authorization': bearer,
          'Content-Type': 'application/json'
      }
  })
		.then(function(response){
			return response.json();

		})
		.then(
			(json) => {
  setMenu(json)
});
    }

	},[props.loggedIn]);
  return(
    //   <><nav className={`navbar ${props.className} navbar-expand-lg navbar-light bg-light`}>
    //   <Col lg={9} md={9}>
    //     <ul className="navbar-nav">
    //       {
    //         props.user && props.user.username && props.loggedIn === true? 
    //         menu.length > 0 && menu.map((menuItem)=> {
    //           return <li key={menuItem.key} className="nav-item">
    //           <Link href ={menuItem.relative}>
    //           <a className="nav-link">
    //             {menuItem.title}
    //           </a>
    //           </Link>
    //         </li>
    //       })
    //     : 
    //           menu.slice(2,2).map((menus) => {
    //           return <li key={menus.key} className="nav-item">
    //           <Link href ={menus.relative}>
    //           <a className="nav-link">
    //             {menus.title}
    //           </a>
    //           </Link>
    //         </li>
    //         }) 
    //       }
    //      </ul>

    //   </Col>
    // </nav>
    // {openModal === true ?
    // <FavouriteButtonModal isOpen={openModal} close={undefined}  data={undefined}/> : null}
    // </>

    <Nav className="me-auto" style={{width:"-webkit-fill-available"}}>
       {
            props.user && props.user.username && props.loggedIn === true? 
            menu.length > 0 && menu.map((menuItem)=> {
              return <Link href={menuItem.relative}>
               <Nav.Link as="a" href={menuItem.relative}>{menuItem.title}</Nav.Link>
               </Link>
          })
        : 
              menu.slice(2,2).map((menuItem) => {
              return <Link href={menuItem.relative}>
              <Nav.Link as="a" href={menuItem.relative}>{menuItem.title}</Nav.Link>
              </Link>
            }) 
          }
    </Nav>
)

  }

export default NavBar;
import React, { useState } from "react";
import { Accordion, Modal } from "react-bootstrap";
import CardPharma from "./Card";
import { useEffect } from "react";


interface Iprops {
  open: boolean;
}

function Accordian(AccordianData,props: Iprops) {
  const [show, setShow] = useState(props.open);
  return (
    <>
    
    {AccordianData.AccordianData &&
          AccordianData.AccordianData.map((modalData: any, index: number) => (
          <>
            <div className = "container "style={{marginTop: "20px"}}>
              <Accordion>
                <Accordion.Item eventKey="0">
                  <Accordion.Header>{modalData.title}</Accordion.Header>
                  <Accordion.Body>
                  <CardPharma url={`${global.config.variables.api_url_base}${modalData.field_accordian_image}`} />
                  <div dangerouslySetInnerHTML={{__html:modalData.body}}></div>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>   
            </div>
            </>
          ))}
            
    </>
  );
}

export default Accordian;
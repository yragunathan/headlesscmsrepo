import React, { useEffect, useState } from 'react';


import Image from 'next/image';
import logo from '../public/assets/logo.png'
import { Col, Row, Navbar, Nav } from 'react-bootstrap';
import { Button } from './Button';
import { FaTwitter, FaFacebookSquare, FaInstagram } from 'react-icons/fa'
import Login from './LoginPopup';
import { LoginData } from './types/App.types';
import NavBar from './NavBar';
import { useRouter } from 'next/router';
import SignUp from './SignUp';
import ToastMsg from './ToastMsg';

type User = {
  name: string;
};

interface HeaderProps {
  user?: User;
  onLogin: () => void;
  onLogout: () => void;
  onCreateAccount?: () => void;
}

export const IHeader = ({ user, onLogout }: HeaderProps) => {
    const [loggedIn, setLoggedIn] = useState(false);
    const [show, setShow] = useState(false);
    const [loginResp, setLoginResp] = useState({} as any);
    const [logPopup, setLogPopup] = useState(false);
    const [openSignup, setOpenSignup] = useState(false);
    const [userData,setUserData] = useState({} as LoginData)
    const [logoData,setLogoData] = useState([] as any);
    const imgLogo = logoData && logoData[0] && logoData[0].field_logo_image;
    const router = useRouter();
    const dataStore = loginResp && loginResp.status === 200 ? typeof window !== 'undefined' && window.localStorage.getItem('token-info') : '';
 
    const logindata = dataStore !== '' ? JSON.parse(dataStore) : null;
    const handleLogin = () => {
      setLogPopup(true);
    }
    const handleLoginClose = () => {
       setLogPopup(false)
      if (loginResp.status !== 200){
        setLoginResp({})
      }
    }
    const handleCallback = (childData) =>{
      setUserData(childData)
    }
    const handleSignup = () => {
      setOpenSignup(true)
    }
    const handleSignupClose = () => {
      setOpenSignup(false)
    }
  const loginForm = () => {
    var formData_token = new FormData();
    Object.keys(userData).forEach((key)=>{
      formData_token.append(key, userData[key]);
    });
    localStorage.setItem('token-info', JSON.stringify(userData));
    fetch(global.config.variables.api_url_base+"/oauth/token", {
        method: 'POST',
        body: formData_token,
    })
    .then(response => {setLoginResp(response); return response.json()})
    .then(data => {
          setLoggedIn(true);
          setLogPopup(false);
          localStorage.setItem("token", JSON.stringify(data));
    })
  }

  
  useEffect(() => {  
  if ( dataStore !== null  && dataStore !== ""){
    setLoggedIn(true);
  } 
    fetch(global.config.variables.api_url_base + "/logo_rest")
    .then((res) => res.json())
    .then((logoData) => {
      setLogoData(logoData);
    });
  

  }, [dataStore]);
   const handleLogout = () => {
    localStorage.removeItem('token-info');
    localStorage.removeItem('token');
    setLoggedIn(false);
    router.push('/')
    setLoginResp({})
   }
   useEffect(() => {
     if(loginResp && loginResp.status === 200){
      setShow(true)
     }
     setTimeout(() => {
       setShow(false)
     }, 3000);
   },[loginResp])
  return(
  // <><div className='navbar-light bg-light nav__sticky'>
  //     <div className="wrapper">
  //     <Col lg={2} md={2} className='nav-30'>
  //     <a className="navbar-brand">
  //        <Image src={logo} alt='iPharma' loading="lazy" style={{width :"80px",minHeight:"auto", maxHeight :"auto", height:"auto"}}/> 
  //     </a>
  //     </Col>
  //     <Col  lg={8} md={8} sm={8} >
  //      <NavBar className='hide' user={logindata} loggedIn ={loggedIn}/>
  //      </Col>
  //       <Col lg={2} md={2}>
  //         {logindata && logindata.username && loggedIn ? (
  //           <>
  //             <span className="welcome">
  //               Welcome, <b>{logindata.username}</b>!
  //             </span>
  //             <button onClick={handleLogout}>logout</button>
  //           </>
  //         ) : (
  //           <>
  //             <Button size="small" onClick={handleLogin}  label="Log in" />
  //             <Button size='small' onClick={handleSignup} label='Sign Up' />
  //           </>
  //         )}
  //       </Col>
       
  //     </div>
  //   </div>
  //   <Login open={logPopup}  close={handleLoginClose} handleLoginData={handleCallback} loginForm={loginForm} loginResp={loginResp}/>
  //   <SignUp open={openSignup} close={handleSignupClose} />
  //   {show ? 
  //   <ToastMsg id='toast' msg={'you are logged in'}  />
  //   : null}
  //   </>


  <Navbar  collapseOnSelect bg="light" expand="md" fixed="top">
    <Navbar.Toggle aria-controls="basic-navbar-nav" style={{borderColor:"rgba(0,0,0,0)", boxShadow:"0 0 0 0" }} />
     <Navbar.Brand href="/" style={{maxWidth :"12rem", marginTop:"0.5rem"}}>
      <Image src={logo} alt='iPharma' loading="lazy"/> 
      </Navbar.Brand>
     
     <Navbar.Collapse id="basic-navbar-nav">
     <NavBar className='hide' user={logindata} loggedIn ={loggedIn}/>
     <Nav>
           
          
    

     
     {logindata && logindata.username && loggedIn ? (
            <>
            <Nav.Link>
              <span className="welcome">
                Welcome, <b>{logindata.username}</b>!
              </span>
              </Nav.Link>
              <Nav.Link eventKey={3}>
              <button onClick={handleLogout}>logout</button>
              </Nav.Link>
            </>
          ) : (
            <>
            <Nav.Link eventKey={1}>
              <Button size="small" onClick={handleLogin}  label="Log in" />
              </Nav.Link>
              <Nav.Link eventKey={2}>
              <Button size='small' onClick={handleSignup} label='Sign Up'/>
              </Nav.Link>
            </>
          )}
          </Nav>
          </Navbar.Collapse>
   <Login open={logPopup}  close={handleLoginClose} handleLoginData={handleCallback} loginForm={loginForm} loginResp={loginResp}/>
     <SignUp open={openSignup} close={handleSignupClose} />
    {show ? 
   <ToastMsg id='toast' msg={'you are logged in'}  />
    : null}
  </Navbar>
  )};
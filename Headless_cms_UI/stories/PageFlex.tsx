import React from 'react';
import { Col, Container, Row } from "react-bootstrap";

import { IHeader } from './Header';
import IPharmaCarousel from './Carousel';
import FreeContent from './FreeContent';
import Kaltura from './Kaltura';
import SalesCards from './SalesCards';
import Accordian from './Accordian';
import Footer from './Footer';

import './PageFlex.css';
import '../sass/themeA.scss';
import '../sass/subthemeB.scss';

type User = {
  name: string;
};

const ipharma_args = {
  BannerData: [
    {"field_banner_post":"\/sites\/default\/files\/inline-images\/ban-1.png.jpeg"},{"field_banner_post":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"}
  ]
}

const accr_args = {
  AccordianData: [
  {"title": "Accordian 1", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
  {"title": "Accordian 2", "body": "Nunc fermentum erat enim, ut pellentesque lectus egestas sed. Aliquam suscipit dignissim euismod. Sed condimentum commodo turpis in venenatis. Vestibulum blandit pulvinar ante, vitae accumsan urna. Ut purus arcu, sagittis sed feugiat at, bibendum et nulla. Nunc elementum fringilla risus, in lacinia augue lacinia quis. Suspendisse malesuada molestie massa, vel vehicula erat facilisis sed. Mauris consequat et velit sed consequat. Nulla nec cursus augue, nec porta leo. Phasellus sed porttitor urna, vel scelerisque risus. Phasellus elementum eget mauris at mattis. Aliquam et nisi lacinia, sollicitudin tortor non, consectetur enim. Sed porttitor sodales molestie. Fusce vel dui eu nisl consequat tempor in a sem. Nullam lobortis consequat lacus sit amet bibendum. Donec nec semper mi.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
  {"title": "Accordian 3", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
] }

export const PageFlex: React.VFC = () => {
  const [user, setUser] = React.useState<User>();
  return (
    <article>
      <IHeader
        user={user}
        onLogin={() => setUser({ name: 'Jane Doe' })}
        onLogout={() => setUser(undefined)}
        onCreateAccount={() => setUser({ name: 'Jane Doe' })}
      />
     <IPharmaCarousel {...ipharma_args}  />
 <Container>
    <Row>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column one</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column two</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column three</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column four</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
    </Row>
    <p>&nbsp;</p>
   <Row>
     <Col md={4} sm={12}>
      <FreeContent content="Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
     </Col>
     <Col  md={4} sm={12}>
      <FreeContent content="
        <p>In this molecule, we do a simple test for the parent-child theming, implemented via SASS.</p>
        <h3 class='themeA'>This is h3 in parent theme A</h3>
        <h3 class='themeA subthemeB'>This is h3 in child subtheme B</h3>
        <div class='btn btn-themeA'>Button defined in parent theme A</div>
        <div class='btn btn-themeA btn-subthemeB'>Button defined in child subtheme B</div>
        <br><br>
        <div class='alert alert-themeA'>Alert defined in parent theme A</div>
        <div class='alert alert-themeA alert-subthemeB'>Alert defined in child subtheme B</div>
        " />
     </Col>
     <Col md={4} sm={12}>
       <Accordian {...accr_args} />
     </Col>
   </Row>
   <p>&nbsp;</p>
   <Row>
    <Col md={6} sm={12}>
        <Kaltura url="https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag" />
    </Col>
    <Col md={6} sm={12}>
        <Kaltura url="https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag" />
    </Col>
   </Row>
   <p>&nbsp;</p>

   <Row>
    <Col>
        <SalesCards />
    </Col>
   </Row>

 </Container>
 <p>&nbsp;</p>
      <Footer />
      
    </article>
  );
};

export default PageFlex;

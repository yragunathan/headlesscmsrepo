import React, { useState } from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Login from './LoginPopup'
import { LoginData } from './types/App.types';

export default {
  title: 'Molecules/login',
  component: Login,
  
} as ComponentMeta<typeof Login>;

const Template: ComponentStory<typeof Login> = () => <Login  open={logPopup} close={handleLoginClose} handleLoginData={handleCallback} loginForm={loginForm} />;

const [loggedIn, setLoggedIn] = useState(false);
const [logPopup, setLogPopup] = useState(false);
const [userData,setUserData] = useState({} as LoginData)


const handleLogin = () => {
  setLogPopup(true);
}
const handleLoginClose = () => {
  setLogPopup(false)
}
const handleCallback = (childData) =>{
  setUserData(childData)
}


const loginForm = () => {
  var formData_token = new FormData();
}

export const login = Template.bind({});

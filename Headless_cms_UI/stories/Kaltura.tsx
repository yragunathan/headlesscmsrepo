import React, { useEffect, useState } from "react";

interface Iprops {
  url: string;
}

const Kaltura = (props: Iprops) =>  (
    <div className="video-container" style={{position:'relative',paddingBottom:'56.25%',paddingTop:'30px',height:'0',overflow:'hidden'}}>
     <iframe id='kaltura_player_1654580996' src={props.url}  allowFullScreen allow='autoplay *; fullscreen *; encrypted-media *' frameBorder='0' style={{position:'absolute',top:'0',left:'0',width:'100%',height:'100%'}}></iframe>
    </div> 
  );



export default Kaltura;

import React, { useEffect, useState } from 'react';
import { SSRProvider, Tab, Tabs } from 'react-bootstrap';
import UserList from './lists';


function TabbedContent() {
    const [key, setKey] = useState('active');
    const [tableData, setTableData] = useState([] as any);
    useEffect(() => {
      fetch(global.config.variables.api_url_base + "/lists_rest_view")
    .then((res) => res.json())
    .then((data) => setTableData(data))
    },[])
   
    return (
      <SSRProvider>
      <Tabs
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        className="mb-3">

        <Tab eventKey="active" title="Active">
          <UserList data={tableData.filter((item) => item.field_status === 'Active')} />
        </Tab>
        <Tab eventKey="inactive" title="Inactive">
        <UserList data={tableData.filter((item) => item.field_status === 'Inactive')} />
        </Tab>
        <Tab eventKey="current" title="Current">
        <UserList data={tableData.filter((item) => item.field_status === 'Current')} />
        </Tab>
      </Tabs>
      </SSRProvider>
    );
  }
  
    
export default TabbedContent;


import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { within, userEvent } from '@storybook/testing-library';
import Kaltura from './Kaltura';

export default {
  title: 'Molecules/Kaltura',
  component: Kaltura,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof Kaltura>;

const Template: ComponentStory<typeof Kaltura> = (args) => <Kaltura {...args} />;

export const kaltura = Template.bind({});
kaltura.args = {
    "url": "https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag"
}
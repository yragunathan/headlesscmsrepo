import React from 'react';
import { Card } from 'react-bootstrap';
import Image from "next/image";

interface cardProps{
    width?: string;
    url:any;
    height?:string;
    className?:string
}

const CardPharma =(props: cardProps) => {
    return(
        <Card>
            <Image className={props.className} loading="lazy" width="150" height="300" src={props.url} />
       </Card>
    )
    };
    
export default CardPharma;


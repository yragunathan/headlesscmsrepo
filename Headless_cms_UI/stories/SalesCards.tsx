import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import CardPharma from "./Card";
import { useEffect } from "react";
import HeroText from "./HeroText";


const SalesCards = (props) => {
  /*const [data, setData] = useState([]);

  useEffect(() => {
      fetch(global.config.variables.api_url_base + "/featuredcards_rest")
        .then((res) => res.json())
        .then((data) => {
          setData(data);
})
  }, []);*/

  return (
    <div className="p-x-30 m-b-30 m-r-40">
        <Row>  <HeroText /> </Row>
      <Row className="align-cards p-x-30" xl={12} lg={12} md={12} sm={12}>
        Best selling products
      </Row>
      <Row className="align-cards">
        {props.FeaturedCardsData && props.FeaturedCardsData.FeaturedCards &&
          props.FeaturedCardsData.FeaturedCards.map((card: any, index: number) => (
            <>
              <Col  lg={3} md={4} sm={6} xs={12} className="p-x-30">
                <CardPharma key={index} url={`${global.config.variables.api_url_base}${card.field_image_cards}`} />
                <div className="category text-center">{card.field_category}</div>
                <div className="text-center" dangerouslySetInnerHTML={{__html: card.title}}></div>
                <div className="text-center">{card.field_price}</div>
              </Col>
            </>
          ))}
      </Row>
      <Row></Row>
    </div>
  );
};

export default SalesCards;

import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import ResetPassword  from '../pages/ResetPassword';

export default {
  title: 'Molecules/restPassword',
  component: ResetPassword,
  
} as ComponentMeta<typeof ResetPassword>;

const Template: ComponentStory<typeof ResetPassword> = () => <ResetPassword />;

export const restPassword = Template.bind({})

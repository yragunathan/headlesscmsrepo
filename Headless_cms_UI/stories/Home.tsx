import react from "react";
import IPharmaCarousel from "./Carousel";
import Footer from "./Footer";
import SalesCards from "./SalesCards";

const Home = () => (
  <>
    <IPharmaCarousel />
    <SalesCards />
  </>
);
export default Home;

import React from 'react';

import { IHeader } from './Header';
import IPharmaCarousel from './Carousel';
import SalesCards from './SalesCards';
import Accordian from './Accordian';
import Footer from './Footer';
import Click2Call from './Click2Call';

import './page.css';

type User = {
  name: string;
};

const ipharma_args = {
  BannerData: [
    {"field_banner_post":"\/sites\/default\/files\/inline-images\/ban-1.png.jpeg"},{"field_banner_post":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"}
  ]
}

const accr_args = {
  AccordianData: [
  {"title": "Accordian 1", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
  {"title": "Accordian 2", "body": "Nunc fermentum erat enim, ut pellentesque lectus egestas sed. Aliquam suscipit dignissim euismod. Sed condimentum commodo turpis in venenatis. Vestibulum blandit pulvinar ante, vitae accumsan urna. Ut purus arcu, sagittis sed feugiat at, bibendum et nulla. Nunc elementum fringilla risus, in lacinia augue lacinia quis. Suspendisse malesuada molestie massa, vel vehicula erat facilisis sed. Mauris consequat et velit sed consequat. Nulla nec cursus augue, nec porta leo. Phasellus sed porttitor urna, vel scelerisque risus. Phasellus elementum eget mauris at mattis. Aliquam et nisi lacinia, sollicitudin tortor non, consectetur enim. Sed porttitor sodales molestie. Fusce vel dui eu nisl consequat tempor in a sem. Nullam lobortis consequat lacus sit amet bibendum. Donec nec semper mi.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
  {"title": "Accordian 3", "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet lectus nulla, nec pulvinar massa dignissim quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aliquam efficitur tempus ullamcorper. Morbi vestibulum iaculis lobortis. Praesent sollicitudin velit ac sem dapibus feugiat. Nullam dictum suscipit orci, id lacinia diam viverra sed. Morbi venenatis bibendum quam, vel lobortis turpis ultrices id. Proin eget blandit ex. Vivamus et euismod dolor, sed scelerisque mi. Donec rutrum venenatis purus, non ultrices risus scelerisque at.","field_accordian_image":"\/sites\/default\/files\/inline-images\/h100_desktop-min.png"},
] }

export const Page: React.VFC = () => {
  const [user, setUser] = React.useState<User>();
  return (
    <article>
      <IHeader
        user={user}
        onLogin={() => setUser({ name: 'Jane Doe' })}
        onLogout={() => setUser(undefined)}
        onCreateAccount={() => setUser({ name: 'Jane Doe' })}
      />
      <IPharmaCarousel {...ipharma_args} />
      <SalesCards />
      <Footer />
      
    </article>
  );
};

export default Page;

import React, { useState } from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Modal from './Modal';

export default {
  title: 'Molecules/Modal',
  component: Modal,
  
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = () => <Modal isOpen={modalOpen} close={setModaltoOpenFalse} data = {""} />

const [modalOpen, setModalOpen] = useState(false);
	const setModaltoOpenTrue = () => {
		setModalOpen(true);
	}
	const setModaltoOpenFalse = () => {
		setModalOpen(false);
	}


export const accordian = Template.bind({})

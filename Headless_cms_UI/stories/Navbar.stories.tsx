import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import NavBar  from './NavBar';

export default {
  title: 'Molecules/Nav',
  component: NavBar,
  
} as ComponentMeta<typeof NavBar>;

const Template: ComponentStory<typeof NavBar> = () => <NavBar loggedIn={false} />;

export const navbar = Template.bind({})

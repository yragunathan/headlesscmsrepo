import Link from "next/link";
import { useCallback, useEffect, useState } from "react";
import { Alert, Button, Form, FormControlProps, Modal } from "react-bootstrap";
import { LoginData } from "./types/App.types";

interface fpwdProps{
  open:boolean;
  handleClose:any;
}

const ForgotPassword = (props:fpwdProps) => {
 const[forgotPwd, setforgotPwd] = useState(props.open)
const [email, setEmail] = useState('');
const [fwresp, setFwResp] = useState({} as any)
const handleClose = useCallback(() => setforgotPwd(props.handleClose()),[props]);
const handleSubmit = () => {
  fetch(global.config.variables.api_url_base + "/user/lost-password?_format=json", {
    method: 'POST',
    headers:{
      "content-type" : "application/json",
    },
    body: JSON.stringify({
      mail: email
    }),
})
    .then((res) => setFwResp(res))
}
return (
<>
<Modal show={props.open} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Forgot Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {fwresp.status === 200 ? 
            <Alert variant="success">Please check your mail for temporary password</Alert> : null}
            {fwresp && String(fwresp.status)[0] === '4' ?
            <Alert variant='danger'>{fwresp.statusText}</Alert>: null}
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="text"
                  name="email"
                  placeholder="Enter mail"
                  onChange={(e) => setEmail(e.target.value)} />
              </Form.Group>
            </Form>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={handleSubmit}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
        </>
)
}

export default ForgotPassword;
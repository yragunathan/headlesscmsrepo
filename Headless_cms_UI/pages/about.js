import IPharmaCarousel from "../stories/Carousel";
import Footer from "../stories/Footer";
import { IHeader } from '../stories/Header';
import NavBar from '../stories/NavBar';
import getAboutData from '../lib/AboutRest'
import getAboutAboutReadMoreData from '../lib/AboutReadMore'
import About from '../stories/About'
import TabbedContent from '../stories/TabbedContent'
import { Container, Row } from "react-bootstrap";

export async function getStaticProps() {
  const AboutReadMore = await getAboutAboutReadMoreData()
  const AboutData = await  getAboutData()

  const BothAboutAndMoreData = { AboutData:AboutData,
    AboutReadMore:AboutReadMore};
  return {
    props: {
      BothAboutAndMoreData
    }
  }
}

const about = ({BothAboutAndMoreData}) => (
  <>
  <About BothAboutAndMoreData={BothAboutAndMoreData} />
  </>
);
export default about;


import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import { Alert, Button, Card, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import * as yup from 'yup';


const ResetPassword = () => {
  const [resetForm, setReset] = useState({
    name: "",
    temp_pass: "",
    new_pass: "",
    cnfm_pass: "",
  });
  const [resetResp, setResetResp] = useState({} as any);
  const {Group, Label, Control} = {...Form}
  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setReset({ ...resetForm, [name]: value });
  };
  const resetSchema = yup.object().shape({
    "new_pass": yup.string().required('Password is required').min(8, "Password must be 8 characters long").max(20, "Password must be maximum 20 characters long"),
    "passwordConfirmation": yup.string().oneOf([yup.ref('new_pass'), null], 'Passwords must match')

  })
  

  const { register, handleSubmit,reset, formState:{errors} } = useForm({resolver: yupResolver(resetSchema)});
  const onSubmit = () => {
  
    fetch(global.config.variables.api_url_base+"/user/lost-password-reset?_format=json", {
      method: 'POST',
      headers:{
        "content-type" : "application/json",
      },
       body: JSON.stringify({name:resetForm.name, temp_pass:resetForm.temp_pass, new_pass: resetForm.new_pass}),
  }).then((res) => setResetResp(res))
  reset();
  }

  const onError = (errors) => {
    console.error(errors, 'error');
  };
  return (
    <div className="reset-password">
      <Card style={{ width: "25rem" }}>
        <Card.Header>Reset Password</Card.Header>
        <Card.Body>
          <form onSubmit={ handleSubmit(onSubmit, onError )}>
          {resetResp.status === 200 ?
            <Alert variant="success">
              Password changed Successfully!! Please login
            </Alert>
            : null}
            {resetResp && String(resetResp.status)[0] === '4'  ?
            <Alert variant="danger">
              {resetResp.statusText}
            </Alert>
            : null
        }
            <Group className="mb-3" controlId="email">
              <Label>Username</Label>
              <Control
                type="text"
                name="name"
                onChange={(e:any) => {console.log(e.target.value); setReset({...resetForm, name: e.target.value })}}
                placeholder="Enter username"
              />
            </Group>
            <Group className="mb-3" controlId="tempPass">
              <Label>Current Password</Label>
              <Control
                type="password"
                name="temp_pass"
                onChange={(e:any) => {console.log(e.target.value); setReset({...resetForm, temp_pass: e.target.value })}}
                placeholder="Current Password"
              />
            </Group>
            <Group className="mb-3" controlId="newPass">
              <Label>New Password</Label>
              <Control
                type="password"
                name="new_pass"
                onChange={(e:any) => {console.log(e.target.value); setReset({...resetForm, new_pass: e.target.value })}}
                placeholder="New Password"
               {...register('new_pass')}
              />
               {errors.new_pass && <p className="errors">{errors.new_pass.message}</p>}
            </Group>
            <Group className="mb-3" controlId="cnfmPass">
              <Label>Confirm Password</Label>
              <Control
                type="password"
                name="cnfm_pass"
                onChange={(e:any) => {console.log(e.target.value); setReset({...resetForm, cnfm_pass: e.target.value })}}
                placeholder="Confirm Password"
                {...register('passwordConfirmation')}
              />
               {errors.passwordConfirmation && <p className="errors">{errors.passwordConfirmation.message}</p>}

            </Group>
            <button className="btn" type="submit">Reset Password</button>
          </form>
        </Card.Body>
      </Card>
    </div>
  );
};

export default ResetPassword;

import '../styles/globals.scss'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../pages/api/global.js'

import "../sass/themeA.scss";
import "../sass/subthemeB.scss";

import Layout from './layout';
function MyApp({ Component, pageProps }) {
  return (
  <Layout>
  <Component {...pageProps} />
  </Layout>
  )
}

export default MyApp

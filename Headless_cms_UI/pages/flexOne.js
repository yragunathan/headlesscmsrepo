
import IPharmaCarousel from '../stories/Carousel';
import Footer from '../stories/Footer';
import { IHeader } from '../stories/Header';
import NavBar from '../stories/NavBar';
import SalesCards from '../stories/SalesCards';
import getBannerData from '../lib/BannerRest'
import getAccordianData from '../lib/AccordianRest';
import { Col, Container, Row } from "react-bootstrap";
import FreeContent from '../stories/FreeContent';
import Accordian from '../stories/Accordian';
import Kaltura from '../stories/Kaltura';

export async function getStaticProps() {
  const BannerData =await  getBannerData()
  const AccordianData = await getAccordianData()
  return {
    props: {
      BannerData,
      AccordianData
    },
    revalidate: true, 
  }
}
export default function FlexOne({ BannerData, AccordianData }) {
  return (
  <div className={"App"}>
 <IPharmaCarousel  BannerData ={BannerData}  />
 <Container>
   <Row>
     <Col md={5} sm={12}>
      <SalesCards />
     </Col>
     <Col  md={3} sm={12}>
      <FreeContent content="Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge). <br> <b>Personalized is key:</b> The Swiss Big Pharma is looking to go beyond the traditional digital marketing foray, which includes webinars, email campaigns, doctor portals and generic medical YouTube videos, to this on-demand, tailored video/audio service. The content is created by Novartis with rheumatologists and other healthcare professionals to deliver what they see as the best video service for practicing rheumatologists in the field. Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. " />
     </Col>
     <Col md={4} sm={12}>
       <Accordian AccordianData = {AccordianData} />
     </Col>
   </Row>
   <Row>
     <Col>
      <Kaltura url="https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag" />
     </Col>
   </Row>
   <Row>
     <Col md={6} sm={12}>
      <FreeContent content="<hr>The content is created by Novartis with rheumatologists and other healthcare professionals to deliver what they see as the best video service for practicing rheumatologists in the field. Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge). Personalized is key: The Swiss Big Pharma is looking to go beyond the traditional digital marketing foray, which includes webinars, email campaigns, doctor portals and generic medical YouTube videos, to this on-demand, tailored video/audio service.The content is created by Novartis with rheumatologists and other healthcare professionals to deliver what they see as the best video service for practicing rheumatologists in the field. Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style." />
    </Col>
    <Col md={6} sm={12}>
      <FreeContent content="<hr>The Swiss Big Pharma is looking to go beyond the traditional digital marketing foray, which includes webinars, email campaigns, doctor portals and generic medical YouTube videos, to this on-demand, tailored video/audio service.The content is created by Novartis with rheumatologists and other healthcare professionals to deliver what they see as the best video service for practicing rheumatologists in the field. Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style." />
    </Col>
   </Row>
 </Container>
 <p>&nbsp;</p>
 </div>
)
  }
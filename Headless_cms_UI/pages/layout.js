import {IHeader} from '../stories/Header';
import Footer from '../stories/Footer';

const Layout = ({ children }) => {
    return (
        <>
          <IHeader />
          <main>
           {children}
           </main>
          <Footer />
        </>
      );
}

export default Layout;
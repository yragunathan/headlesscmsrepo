
import IPharmaCarousel from '../stories/Carousel';
import Footer from '../stories/Footer';
import { IHeader } from '../stories/Header';
import NavBar from '../stories/NavBar';
import SalesCards from '../stories/SalesCards';
import getBannerData from '../lib/BannerRest'
import getFeaturedCardsData from '../lib/FeaturedCardsRest'
import getMetadataForFeatureCards from '../lib/MetadataFeatureCards';
import GoTop from '../stories/GoTop';
import Head  from 'next/head';

export async function getStaticProps() 
{
  const FeaturedCardsData = await getFeaturedCardsData();
  const BannerData =await  getBannerData();
  const MetaData = await getMetadataForFeatureCards();

  const BothBannerAndFeatureCardsData = { FeaturedCards:FeaturedCardsData,
    Banner:BannerData,
  MetaData:MetaData};


  return {
    props: {
      BothBannerAndFeatureCardsData
    },
    revalidate: true, 
  }
}
export default function Home({ BothBannerAndFeatureCardsData }) {
  return (
    <>
    { <Head>
      <title>{BothBannerAndFeatureCardsData.MetaData.title}</title>
      <meta name="description" content={BothBannerAndFeatureCardsData.MetaData.keywords.split(",").join("|")}></meta>
    </Head>}
<div className="App">
 <IPharmaCarousel  BannerData ={BothBannerAndFeatureCardsData}  />
<SalesCards FeaturedCardsData = {BothBannerAndFeatureCardsData}/>
<GoTop />
<p>&nbsp;</p>
 </div>
 </>
)
  }
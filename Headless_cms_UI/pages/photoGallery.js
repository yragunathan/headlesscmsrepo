import React, { useEffect, useState, useRef } from 'react';

const PhotoGallery = () => {

    const [data, setData] = useState([]);
    const [imageIndex, setImageIndex] = useState(1);
    const refs = useRef([]);
    const trefs = useRef([]);
    const divRef = useRef();
    let log = console.log;

    useEffect(() => {
        fetch(global.config.variables.api_url_base + "/featuredcards_rest")
            .then((res) => res.json())
            .then((data) => {
                let rev = data;
                setData([...data]);
            })
    }, []);

    useEffect(() => {
       // trefs.current !== undefined || [] && refs.current !== undefined || [] ? currentImage() : null;
       currentImage();
    }, [ data.length > 0] );

    const currentImage = () => {
        showImages(imageIndex);
    }

    const showImages = (n) => {
        log("im here")
        let index = n;
        if (n > data?.length) {
            index = 1;
        }
        if (n < 1) {
            index = data?.length;
        }

        for (let i = 0; i < data.length; i++) {
            refs.current[i].style.display = "none";
            trefs.current[i].className = trefs.current[i].className.replace(" active", "");
        }
        if (refs.current.length > 0) {
            refs.current[index - 1].style.display = "block";
            trefs.current[index - 1].className += " active";
        }
        setImageIndex(index);
    }

    
    const slideThumnailNext =  () => {
    console.log(divRef, "div ref")
    let container = divRef.current;
   sideScroll(container,'right',40,450,70);
};


const slideThumnailBack =  () => {
    console.log(divRef, "div ref")
    let container = divRef.current;
   sideScroll(container,'left',40,450,70);
};

function sideScroll(element,direction,speed,distance,step){
   let scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

    return (
        <>
            <h3 style={{ "text-align": "center", "padding-top": "5px" }}>Photo Gallery</h3>
            <div className="gallery__flex">
                <div className="gallery__container">
                    <div className='tumbnailContainer'>
                    <div className="gallery__row" ref={divRef}>
                        {/* <div className="gallery__column">
                            <a className="thumbnail__prev" onClick={() => imageLoader('prev')}>❮</a>
                        </div> */}
                        {data.length > 0 && data.map((obj, index) => {
                            return (
                                <>
                                    <div className="gallery__column">
                                        <img ref={(element) => { trefs.current[index] = element }} className='gallery__cursor thumbnail__shade' src={`${global.config.variables.api_url_base}${obj.field_image_cards}`} style={{ width: "100%" }} onClick={() => showImages(index + 1)} />
                                    </div>
                                </>
                            )
                        })}
                         </div>
                        {data.length > 0 &&
                        <>
                            <a className="thumbnail__prev" onClick={() => slideThumnailBack()}>❮</a>
                            <a className="thumbnail__next" onClick={() => slideThumnailNext()}>❯</a>
                        </>
                    }
                   
                    </div>

                    {data.length > 0 && data.map((obj, index) => (
                        <div className="gallery" key={index} ref={(element) => { refs.current[index] = element }}>
                            <img className="gallery__img" src={`${global.config.variables.api_url_base}${obj.field_image_cards}`} style={{ width: "100%" }} />
                        </div>

                    ))}

                    {data.length > 0 &&
                        <>
                            <a className="gallery__prev" onClick={() => showImages(imageIndex - 1)}>❮</a>
                            <a className="gallery__next" onClick={() => showImages(imageIndex + 1)}>❯</a>
                        </>
                    }
                </div>
            </div>
        </>
    )
}

export default PhotoGallery;
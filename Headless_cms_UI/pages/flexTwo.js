
import IPharmaCarousel from '../stories/Carousel';
import Footer from '../stories/Footer';
import { IHeader } from '../stories/Header';
import NavBar from '../stories/NavBar';
import SalesCards from '../stories/SalesCards';
import getBannerData from '../lib/BannerRest'
import getAccordianData from '../lib/AccordianRest';
import { Col, Container, Row } from "react-bootstrap";
import FreeContent from '../stories/FreeContent';
import Accordian from '../stories/Accordian';
import Kaltura from '../stories/Kaltura';

export async function getStaticProps() {
  const BannerData =await  getBannerData()
  const AccordianData = await getAccordianData()
  return {
    props: {
      BannerData,
      AccordianData,
    },
    revalidate: true, 
  }
}
export default function FlexTwo({ BannerData, AccordianData }) {
  return (
  <div className={"App"}>
 <IPharmaCarousel  BannerData ={BannerData}  />
 <Container>
    <Row>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column one</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column two</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column three</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
        <Col md={3} xs={12} sm={12}>
            <FreeContent content="<b>Column four</b><br><br>Novartis is continuing its digital push under CEO Vas Narasimhan with a new Netflix-style approach to help connect with doctors and better learn what content resonates with them. This new service, delivered by software company Evermed, works as tailored, education-based videos aimed specifically at rheumatologists and is known as PEAK (Personalized Education and Knowledge)." />
        </Col>
    </Row>
    <p>&nbsp;</p>
   <Row>
     <Col md={5} sm={12}>
      <SalesCards />
     </Col>
     <Col  md={3} sm={12}>
      <FreeContent content="
        <p>In this molecule, we do a simple test for the parent-child theming, implemented via SASS.</p>
        <h3 class='themeA'>This is h3 in parent theme A</h3>
        <h3 class='themeA subthemeB'>This is h3 in child subtheme B</h3>
        <div class='btn btn-themeA'>Button defined in parent theme A</div>
        <div class='btn btn-themeA btn-subthemeB'>Button defined in child subtheme B</div>
        <br><br>
        <div class='alert alert-themeA'>Alert defined in parent theme A</div>
        <div class='alert alert-themeA alert-subthemeB'>Alert defined in child subtheme B</div>
        " />
     </Col>
     <Col md={4} sm={12}>
       <Accordian AccordianData = { AccordianData } />
     </Col>
   </Row>
   <p>&nbsp;</p>
   <Row>
    <Col md={6} sm={12}>
        <Kaltura url="https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag" />
    </Col>
    <Col md={6} sm={12}>
        <Kaltura url="https://cdnapisec.kaltura.com/p/4615923/sp/461592300/embedIframeJs/uiconf_id/50075763/partner_id/4615923?iframeembed=true&playerId=kaltura_player_1652289734&entry_id=1_0wwitsag" />
    </Col>
   </Row>
   <p>&nbsp;</p>

   <Row>
    <Col>
        <SalesCards />
    </Col>
   </Row>

 </Container>
 <p>&nbsp;</p>
 </div>
)
  }
import react from "react";
import Accordian  from "../stories/Accordian";
import Footer from "../stories/Footer";
import { IHeader } from '../stories/Header';
import NavBar from '../stories/NavBar';
import getAccordianData from "../lib/AccordianRest";
import getMetadataForAccordian from "../lib/MetadataAccordian"
import Head  from 'next/head';
export async function getStaticProps() {
  const AccordianData =await  getAccordianData();
  const MetaData = await getMetadataForAccordian();
  return {
    props: {
      AccordianData,MetaData},
    revalidate: true, 
  }
}

const page = ({ AccordianData ,MetaData}) => (
  <>
  { <Head>
      <title>{MetaData.title}</title>
      <meta name="description" content={MetaData.keywords.split(",").join("|")}></meta>
    </Head>}
<Accordian AccordianData ={AccordianData} />
  </>
);

export default page;


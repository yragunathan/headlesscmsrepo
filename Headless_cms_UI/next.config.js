/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  reactDocgen: false,
  images: {
    domains: ['http://ipharm.axiomthemes.com', '10.4.33.191', 'dummyimage.com'],
  },
  future: {
    webpack5: true
  }
}

module.exports = nextConfig

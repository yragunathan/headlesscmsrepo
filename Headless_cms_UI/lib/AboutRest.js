export default async function getAboutData() {


    const res = await fetch(global.config.variables.api_url_base + "/about?_format=json")
    const data = await res.json()
    return data;
 
}
export default async function getMetadataForAccordian() {


    const res = await fetch(global.config.variables.api_url_base + "/metatags-views/accordian/rest_export_1")
    const data = await res.json()
     return data;
 
}

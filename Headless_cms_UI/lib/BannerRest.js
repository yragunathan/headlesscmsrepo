export default async function getBannerData() {


    const res = await fetch(global.config.variables.api_url_base + "/banner_rest")
    const data = await res.json()
     return data;
 
}
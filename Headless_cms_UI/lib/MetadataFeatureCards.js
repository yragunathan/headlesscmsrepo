export default async function getMetadataForFeatureCards() {


    const res = await fetch(global.config.variables.api_url_base + "/metatags-views/featured_cards/rest_export_1")
    const data = await res.json()
     return data;
 
}

export default async function getFeaturedCardsData() {


    const res = await fetch(global.config.variables.api_url_base + "/featuredcards_rest")
    const data = await res.json()
     return data;
 
}
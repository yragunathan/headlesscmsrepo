export default async function getAboutAboutReadMoreData() {


    const res = await fetch(global.config.variables.api_url_base + "/read_more?_format=json")
    const data = await res.json()
    return data;
 
}
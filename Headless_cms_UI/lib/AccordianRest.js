export default async function getAccordianData() {


    const res = await fetch(global.config.variables.api_url_base + "/accordian_rest")
    const data = await res.json()
    return data;
 
}